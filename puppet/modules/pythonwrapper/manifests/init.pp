# == Class: pythonwrapper
#
# Pip_installer is a wraper around the package and the python::pip class and helps to install
# both the binary packets needed for the python packages as the python packages. 
# It does not check any dependency, that is up to you to provide
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the function of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
# pythonWrapper is just an extension of the official stankevich-python class
#
#  class { pip_installer:
#    binaryPackages => [ 'python2.7-dev', 'liblapack-dev' ],
#    pythonPackages => [ 'numpy' ],
#    version    => 'system',
#    pip        => true,
#    dev        => true,
#    virtualenv => true,
#    gunicorn   => true,
#  }
#
# === Authors
#
# Wessel de Roode <j.w.deroode@gmail.com>
#
# === Copyright
#
# Copyright 2014 Wessel de Roode, Feel free to copy, use or change
#
class pythonwrapper (
  $desktop                    = 'ubuntu-desktop',
  $desktop_minimal_install    = true,
  $useMysql                   = true,
  $usePostgres                = true,
  $securitiesDatabaseUser     = 'sec_user',
  $securitiesDatabasePassword = 'password',
  $binaryPackages             = ['build-essential'],
  $extraPackages              = ['screen'],
  $pythonPackages             = ['numpy', 'scipy'],
  $version                    = 'system',
  $pip                        = true,
  $dev                        = false,
  $virtualenv                 = false,
  $gunicorn                   = false,
  $manage_gunicorn            = true,
  $provider                   = undef
){

  if $desktop_minimal_install == true {

    # Install a minimal desktop
    package { $desktop:
      ensure          => installed,
      install_options => ['--no-install-recommends']
    }->package { 'indicator-session':
      ensure => installed,
    }

  } else {
    
    # Install a fullBlown desktop (takes forever to install)
        package { $desktop:
      ensure => installed,
    }

  }

  #
  # Set the autostart on
  #
  file { [ "/etc/lightdm/", "/etc/lightdm/lightdm.conf.d/" ]:
    ensure  => "directory",
    require => Package[$desktop],
  }->
  file { '/etc/lightdm/lightdm.conf.d/50-myconfig.conf':
    source    => "puppet:///files/etc/lightdm/lightdm.conf.d/50-myconfig.conf",
  }

  #
  # Network manager shut-up work around, else it keeps nagging not started
  #
  package { "network-manager":
    ensure => installed,
    install_options => ['--no-install-recommends']
  }->

  service { "network-manager":
    require => [ Package[$desktop], Package["network-manager"] ],
    ensure => running,
    enable => true,
  }->service { "lightdm":
        require => [ Package[$desktop], File['/etc/lightdm/lightdm.conf.d/'] ],
        ensure  => running,
        enable  => true,
        before  => [ Package['postgresql-client'], Package['mysql_client'], Package['java'] ]
  }
  

  file { "/tmp/desktop-setup":
      source  => "puppet:///files/scripts/desktop-setup",
      recurse => remote,
      mode    => 755,
  } 

  exec { "desktop-setup.sh":
    command => '/bin/bash desktop-setup.sh',
    user    => 'vagrant',
    cwd     => '/tmp/desktop-setup',
    require => [ File["/tmp/desktop-setup"], Service["lightdm"] ],
    }    

  user { 'ubuntu':
    ensure           => 'present',
    password         => '$6$hjzPnQ7d$Q/7euX3fT.ZXse8cG88/IgujBDFibVvZo53xzA2r1puqh8fS6b.gC23jOjhYc8sZ4vKqfD20wKzeYddz3I5zt1',
  }

  class { 'python':
    version         => $version,
    pip             => $pip,
    dev             => $dev,
    virtualenv      => $virtualenv,
    gunicorn        => $gunicorn,
    manage_gunicorn => $manage_gunicorn,
    provider        => $provider,
    require         => [ Package[$desktop], Service["network-manager"] ]
  }->package { $binaryPackages:
        ensure => installed,
  }->python::pip {$pythonPackages:
  }->package { $extraPackages:
        ensure => installed,
  }->
  
  #
  # Create git prompt for every console
  #
  exec { 'git-prompt':
    creates => '/usr/local/share/bash-git-prompt',
    command => 'git clone https://github.com/magicmonty/bash-git-prompt.git /usr/local/share/bash-git-prompt',
  }->file { "/etc/profile.d/git-prompt.sh":
    path => "/etc/profile.d/git-prompt.sh",
    ensure => present,
    source => "puppet:///files/etc/profile.d/git-prompt.sh",
  }


  #
  # Create MySQL Securities Master database
  #
  if $useMysql == true {
    
    class {'mysql::server':
      require => Service['lightdm'],
    }->mysql::db { 'securities_master':
        user     => $securitiesDatabaseUser, 
        password => $securitiesDatabasePassword,
        grant    => ['ALL'],
        sql   => '/tmp/mysql_securities_master.sql',
        require => File["/tmp/mysql_securities_master.sql"]
      }->

    mysql_grant { 'vagrant@localhost/securities_master.*':
      ensure     => 'present',
      options    => ['GRANT'],
      privileges => ['ALL'],
      table      => 'securities_master.*',
      user       => 'vagrant@localhost',
      require    => Mysql::Db['securities_master'],
    }

    file { "/tmp/mysql_securities_master.sql":
      ensure => present,
      source => "puppet:///files/database/mysql/mysql_securities_master.sql",
    }
  } 

  #
  # Create Postgresql Securities Master database
  #  
  if $usePostgres == true {

    class { 'postgresql::server': 
      ensure                     => 'present',
      ip_mask_deny_postgres_user => '0.0.0.0/32',
      ip_mask_allow_all_users    => '0.0.0.0/0',
      listen_addresses           => '*',
      encoding                   => 'UTF8',
      locale                     => 'en_US.UTF-8',
      require => Service['lightdm'],
    }->postgresql::server::db { 'securities_master':
      user      => $securitiesDatabaseUser,
      password  => postgresql_password($securitiesDatabaseUser, $securitiesDatabasePassword),
      encoding  => 'UTF8',
      locale    => 'en_US.UTF-8',
    }
 
    postgresql::server::pg_hba_rule { 'local access to database with MD5 overwriting template rule 002':
      type        => 'local',
      database    => 'all',
      user        => 'all',
      auth_method => 'trust',
      order       => '002',
    }

    postgresql::server::role { 'root':
      #password_hash => postgresql_password('root', 'root'),
      createdb      => true,
      createrole    => true,
      login         => true,
      superuser     => true,
    }

    postgresql::server::database { 'root':
      owner     => 'root',
      encoding  => 'UTF8',
      locale    => 'en_US.UTF-8',
      require   => Postgresql::Server::Role['root'],
    }

    postgresql::server::role { 'vagrant':
      #password_hash => postgresql_password('vagrant', 'vagrant'),
      createdb      => true,
      createrole    => true,
      login         => true,
      superuser     => true,
    }

    postgresql::server::database { 'vagrant':
      owner     => 'vagrant',
      encoding  => 'UTF8',
      locale    => 'en_US.UTF-8',
      require   => Postgresql::Server::Role['vagrant'],
    }
  }
}
