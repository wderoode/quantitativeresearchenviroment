# PyQUANTITATIVE RESEARCH DESKTOP ON UBUNTU 14.x #

This environment was heavily inspired by the work of Mike from [QuantStart](http://www.quantstart.com/) 

His installation manual from [here 1](http://www.ubuntu.com/download/desktop/) and [here 2](http://www.quantstart.com/articles/Quick-Start-Python-Quantitative-Research-Environment-on-Ubuntu-14-04) describes the  Python, Math libs and Databases installation and setup for an excellent *Quantitative Research Environment on Ubuntu Desktop*.
The work that was done here is integrating all those installation manuals into a single Vagrantfile with Puppet & Hiera to make it a full unattended install of the environment within less than an hour depending on your download speed. 


### Requirements ###
* [Oracle VM Virtualbox](https://www.virtualbox.org/wiki/Downloads)
* [Oracle VM VirtualBox Extension Pack](https://www.virtualbox.org/wiki/Downloads)
* [Vagrantup](http://www.vagrantup.com/)
* A CPU running OsX or Windows

### Windows Users, Please Note! Using NFS Synced Folders ###
The current installation uses NFS synced folders (look inside the /Vagrantfile) and is therefor optimised for OsX/Linux hosts. 
If you like to run this Vagrant box on Microsoft (which is no problem) please read this documentation of Vagrant about [NFS Synced Folders](https://docs.vagrantup.com/v2/synced-folders/nfs.html) and how to handle this on Windows


## Quick Start ##
1. Install [Virtualbox](https://www.virtualbox.org/wiki/Downloads),  [VirtualBox Extension Pack](https://www.virtualbox.org/wiki/Downloads) and [Vagrantup](http://www.vagrantup.com/)
2. Clone this repository, and inside run the command: 

```
#!bash

$ vagrant up
```

Vagrant will start by installing a vanilla Ubuntu 14.04 inside the VM. It will bring-up the *Ubuntu Unity Desktop* first and afterwards continue's to install:
1. The python packages
2. Postgresql and MySQL databases, drivers and db accounts used on the quant start website
3. Be patient it takes a long time before it all is completed. (on my slow internet almost 50 minutes)
If the full installation was successful you will see the following screen on your terminal:
```
#!
==> default: Notice: Finished catalog run in 3000 seconds
==> default: =================================================================
==> default: || Puppet Applied! Installation and box Configuration Finished ||
==> default: =================================================================
==> default:  
==> default:  
==> default:  
==> default: You find your host ./ directory inside the VM at /vagrant, and it is always in sync
==> default: Use it with your native editor to work on your projects
==> default:  
==> default: Login with the command line: 
==> default: vagrant ssh
==> default:  
==> default: Login on the GUI with:
==> default: username: vagrant
==> default: password: vagrant
==> default: sudo su -    works :-)
==> default:  
==> default: ROOT account ubuntu:
==> default: username: ubuntu
==> default: password: ubuntu
==> default:  
==> default: Happy Coding! :-)
```
Scroll back the screen and check if no errors occurred.

## Customise the installation ##
Feel free to fork me and change :)


All the configuration variables are placed inside the hiera file:

```
#!bash

/vagrant/puppet/hiera/hieradata/quantstart-configuration.yaml
```
To apply new settings just run the script:

```
#!bash

$ sudo ./apply_puppet.sh
```

The Ubuntu desktop tweaks:
```
#!bash

/vagrant/puppet/manifests/files/scripts/desktop-setup/desktop-setup.sh
```
## Package List ##
Below are the different packages used in this Vagrant box:

### Python Package List ###
        - pip
        - virtualenv
        - gunicorn

        - numpy
        - scipy
        - matplotlib
        - pandas
        - scikit-learn
        - ipython
        - pyzmq
        - pygments
        - patsy
        - statsmodels
        - lxml
        - MySQL-python
        - psycopg2

### Binary Libraries ###
        - build-essential 
        - python2.7-dev
        - liblapack-dev  
        - libblas-dev 
        - libatlas-base-dev 
        - gfortran 
        - libpng-dev 
        - libjpeg8-dev 
        - libfreetype6-dev 
        - libqt4-core 
        - libqt4-gui 
        - libqt4-dev 
        - libzmq-dev
        - libxslt-dev
        - quantlib-python
        - libmysqlclient-dev
        - libpq-dev

### Unity Desktop Apps ###
        - ubuntu-desktop
        - gnome-terminal
        - firefox
        - chromium-browser
        - pgadmin3
        - mysql-workbench
        - vim-gnome
        - geany
        - meld